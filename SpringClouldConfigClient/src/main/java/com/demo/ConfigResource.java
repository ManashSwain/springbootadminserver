package com.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigResource {


	@Value("${msg}")
	private String message;
	
	@Value("${server.port}")
	private int serverPort; 
	
	/*@LocalServerPort
	private int serverPort;*/
	
	
	
	
	@RequestMapping("/config")
	public String getConfigValues() {
		return message+"   "+"comming from server with port::"+serverPort;
	}

}
